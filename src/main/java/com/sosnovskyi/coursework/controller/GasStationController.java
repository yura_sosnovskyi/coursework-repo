package com.sosnovskyi.coursework.controller;

import com.sosnovskyi.coursework.entity.GasStation;
import com.sosnovskyi.coursework.service.GasStationService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("gasStation")
@Data
@RequiredArgsConstructor
public class GasStationController {

    private final GasStationService gasStationService;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping
    public List<GasStation> getAllGasStations() {
        return gasStationService.findAll();
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/{email}")
    public GasStation getGasStation(@PathVariable String email) {
        return gasStationService.findGasStation(email);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public GasStation createGasStation(@RequestBody GasStation gasStation) {
        return gasStationService.save(gasStation);
    }

    @PutMapping(value = "/{email}")
    public GasStation updateUser(@PathVariable String email, @RequestBody GasStation gasStation) {
        return gasStationService.updateGasStation(email, gasStation);
    }

    @DeleteMapping("/{email}")
    public ResponseEntity<Void> deleteGasStationBySurname(@PathVariable String email) {
        gasStationService.deleteGasStation(email);
        return ResponseEntity.noContent().build();
    }
}