package com.sosnovskyi.coursework.controller;

import com.sosnovskyi.coursework.entity.Staff;
import com.sosnovskyi.coursework.service.StaffService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("staff")
@Data
@RequiredArgsConstructor
public class StaffController {

    private final StaffService staffService;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping
    public List<Staff> getAllStaffs() {
        return staffService.findAll();
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/{email}")
    public Staff getStaff(@PathVariable String email) {
        return staffService.findStaff(email);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Staff createStaff(@RequestBody Staff staff) {
        return staffService.save(staff);
    }

    @PutMapping(value = "/{email}")
    public Staff updateUser(@PathVariable String email, @RequestBody Staff staff) {
        return staffService.updateStaff(email, staff);
    }

    @DeleteMapping("/{email}")
    public ResponseEntity<Void> deleteStaffBySurname(@PathVariable String email) {
        staffService.deleteStaff(email);
        return ResponseEntity.noContent().build();
    }
}