package com.sosnovskyi.coursework.controller;

import com.sosnovskyi.coursework.entity.Customer;
import com.sosnovskyi.coursework.service.CustomerService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("customer")
@Data
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService customerService;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping
    public List<Customer> getAllCustomers() {
        return customerService.findAll();
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/{email}")
    public Customer getCustomer(@PathVariable String email) {
        return customerService.findCustomer(email);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Customer createCustomer(@RequestBody Customer customer) {
        return customerService.save(customer);
    }

    @PatchMapping(value = "/{email}")
    public Customer updateUser(@PathVariable String email, @RequestBody Customer customer) {
        return customerService.updateCustomer(email, customer);
    }

    @DeleteMapping("/{email}")
    public ResponseEntity<Void> deleteCustomerBySurname(@PathVariable String email) {
        customerService.deleteCustomer(email);
        return ResponseEntity.noContent().build();
    }
}