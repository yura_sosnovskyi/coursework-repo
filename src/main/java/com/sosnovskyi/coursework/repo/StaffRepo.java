package com.sosnovskyi.coursework.repo;

import com.sosnovskyi.coursework.entity.Staff;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StaffRepo extends JpaRepository<Staff, Integer> {
    Optional<Staff> findByEmail(String email);
}