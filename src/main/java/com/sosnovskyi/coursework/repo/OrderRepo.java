package com.sosnovskyi.coursework.repo;

import com.sosnovskyi.coursework.entity.Order;
import com.sosnovskyi.coursework.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepo extends JpaRepository<Order, Integer> {
}