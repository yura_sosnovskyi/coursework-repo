package com.sosnovskyi.coursework.repo;

import com.sosnovskyi.coursework.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepo extends JpaRepository<Address, Integer> {
}
