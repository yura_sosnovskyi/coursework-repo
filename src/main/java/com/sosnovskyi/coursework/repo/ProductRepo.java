package com.sosnovskyi.coursework.repo;

import com.sosnovskyi.coursework.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProductRepo extends JpaRepository<Product, Integer> {
}