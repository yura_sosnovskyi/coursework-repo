package com.sosnovskyi.coursework.repo;

import com.sosnovskyi.coursework.entity.GasStation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface GasStationRepo extends JpaRepository<GasStation, Integer> {
    Optional<GasStation> findByEmail(String email);
}