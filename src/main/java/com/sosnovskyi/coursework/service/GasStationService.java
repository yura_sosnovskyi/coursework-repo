package com.sosnovskyi.coursework.service;

import com.sosnovskyi.coursework.entity.Address;
import com.sosnovskyi.coursework.entity.GasStation;
import com.sosnovskyi.coursework.exception.EntityNotFoundException;
import com.sosnovskyi.coursework.repo.AddressRepo;
import com.sosnovskyi.coursework.repo.GasStationRepo;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Data
@RequiredArgsConstructor
@Slf4j
public class GasStationService {

    private final GasStationRepo gasStationRepo;
    private final AddressRepo addressRepo;


    @Transactional
    public GasStation save(GasStation gasStation) {
        log.info("save GasStation with {} email...", gasStation.getEmail());

        Address address = gasStation.getAddress();
        if (address.getId() == null) {
            address = addressRepo.save(address);
            gasStation.setAddress(address);
        }
        GasStation savedGasStation = gasStationRepo.save(gasStation);
        log.info("GasStation with {} email, {} id is successfully created",
                savedGasStation.getEmail(), savedGasStation.getId());
        return savedGasStation;
    }

    @Transactional
    public GasStation findGasStation(String email) {
        log.info("findGasStation with {} email...", email);
        GasStation gasStation = gasStationRepo.findByEmail(email)
                .orElseThrow(EntityNotFoundException::new);
        log.info("GasStation with {} email, {} id is successfully found",
                gasStation.getEmail(), gasStation.getId());
        return gasStation;
    }

    @Transactional
    public List<GasStation> findAll() {
        log.info("findAll GasStations");
        List<GasStation> all = gasStationRepo.findAll();
        log.info("gasStation list with {} elements found", all.size());
        return all;
    }

    @Transactional
    public GasStation updateGasStation(String email, GasStation gasStation) {
        log.info("updateGasStation with email {}", email);

        Address address = gasStation.getAddress();
        if (address.getId() == null) {
            address = addressRepo.save(address);
            gasStation.setAddress(address);
        }

        GasStation resGasStation = gasStationRepo.findByEmail(email)
                .map(gasStationer -> {
                    gasStationer.setStaff(gasStation.getStaff());
                    gasStationer.setAddress(gasStation.getAddress());
                    gasStationer.setPhone(gasStation.getPhone());
                    return gasStationRepo.save(gasStationer);
                })
                .orElseGet(() -> gasStationRepo.save(gasStation));
        log.info("gasStation with {} email is successfully updated", resGasStation.getEmail());
        return resGasStation;
    }

    @Transactional
    public void deleteGasStation(String email) {
        log.info("deleteGasStation with email {}", email);
        GasStation gasStation = gasStationRepo.findByEmail(email)
                .orElseThrow(EntityNotFoundException::new);
        gasStationRepo.delete(gasStation);
        log.info("GasStation with {} email is successfully deleted", email);
    }
}
