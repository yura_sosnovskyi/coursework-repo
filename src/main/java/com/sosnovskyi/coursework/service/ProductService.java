package com.sosnovskyi.coursework.service;

import com.sosnovskyi.coursework.entity.Address;
import com.sosnovskyi.coursework.entity.Product;
import com.sosnovskyi.coursework.exception.EntityNotFoundException;
import com.sosnovskyi.coursework.repo.AddressRepo;
import com.sosnovskyi.coursework.repo.ProductRepo;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Data
@RequiredArgsConstructor
@Slf4j
public class ProductService {

    private final ProductRepo productRepo;

    @Transactional
    public Product save(Product product) {
        log.info("save Product with {} id...", product.getId());

        Product savedProduct = productRepo.save(product);
        log.info("Product with {} id is successfully created", savedProduct.getId());
        return savedProduct;
    }

    @Transactional
    public Product findProduct(Integer id) {
        log.info("findProduct with {} id...", id);
        Product product = productRepo.findById(id)
                .orElseThrow(EntityNotFoundException::new);
        log.info("Product with {} id is successfully found", product.getId());
        return product;
    }

    @Transactional
    public List<Product> findAll() {
        log.info("findAll Products");
        List<Product> all = productRepo.findAll();
        log.info("product list with {} elements found", all.size());
        return all;
    }

    @Transactional
    public Product updateProduct(Integer id, Product product) {
        log.info("updateProduct with id {}", id);

        Product resProduct = productRepo.findById(id)
                .map(producter -> {
                    producter.setBrand(product.getBrand());
                    producter.setCategory(product.getCategory());
                    producter.setProductName(product.getProductName());
                    producter.setPrice(product.getPrice());
                    producter.setDescription(product.getDescription());
                    producter.setExpirationDate(product.getExpirationDate());
                    return productRepo.save(producter);
                })
                .orElseGet(() -> productRepo.save(product));
        log.info("product with {} id is successfully updated", resProduct.getId());
        return resProduct;
    }

    @Transactional
    public void deleteProduct(Integer id) {
        log.info("deleteProduct with id {}", id);
        Product product = productRepo.findById(id)
                .orElseThrow(EntityNotFoundException::new);
        productRepo.delete(product);
        log.info("Product with {} id is successfully deleted", id);
    }
}
