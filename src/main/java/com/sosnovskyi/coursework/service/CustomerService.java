package com.sosnovskyi.coursework.service;

import com.sosnovskyi.coursework.entity.Address;
import com.sosnovskyi.coursework.entity.Customer;
import com.sosnovskyi.coursework.exception.EntityNotFoundException;
import com.sosnovskyi.coursework.repo.AddressRepo;
import com.sosnovskyi.coursework.repo.CustomerRepo;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Data
@RequiredArgsConstructor
@Slf4j
public class CustomerService {

    private final CustomerRepo customerRepo;
    private final AddressRepo addressRepo;


    @Transactional
    public Customer save(Customer customer) {
        log.info("save Customer with {} email...", customer.getEmail());
        setCardsAndAddress(customer);
        Customer savedCustomer = customerRepo.save(customer);
        log.info("Customer with {} email, {} id is successfully created",
                savedCustomer.getEmail(), savedCustomer.getId());
        return savedCustomer;
    }

    @Transactional
    public Customer findCustomer(String email) {
        log.info("findCustomer with {} email...", email);
        Customer customer = customerRepo.findByEmail(email)
                .orElseThrow(EntityNotFoundException::new);
        log.info("Customer with {} email, {} id is successfully found",
                customer.getEmail(), customer.getId());
        return customer;
    }

    @Transactional
    public List<Customer> findAll() {
        log.info("findAll Customers");
        List<Customer> all = customerRepo.findAll();
        log.info("customer list with {} elements found", all.size());
        return all;
    }

    @Transactional
    public Customer updateCustomer(String email, Customer customer) {
        log.info("updateCustomer with email {}", email);

        Customer oldCustomer = customerRepo.findByEmail(email)
                .orElseThrow(EntityNotFoundException::new);
        setOldProperties(customer, oldCustomer);

        setCardsAndAddress(customer);
        customer = customerRepo.save(customer);
        log.info("customer with {} email is successfully updated", customer.getEmail());
        return customer;
    }

    @Transactional
    public void deleteCustomer(String email) {
        log.info("deleteCustomer with email {}", email);
        Customer customer = customerRepo.findByEmail(email)
                .orElseThrow(EntityNotFoundException::new);
        customerRepo.delete(customer);
        log.info("Customer with {} email is successfully deleted", email);
    }

    private void setOldProperties(Customer user, Customer oldUser) {

        user.setId(oldUser.getId());
        user.setName(oldUser.getName());
        user.setEmail(oldUser.getEmail());

        if (user.getPhone() == null) {
            user.setPhone(oldUser.getPhone());
        }
        if (user.getSurname() == null)
            user.setSurname(oldUser.getSurname());
        if (user.getCreditCards() == null)
            user.setCreditCards(oldUser.getCreditCards());
        if (user.getAddress() == null)
            user.setAddress(oldUser.getAddress());
        if (user.getDiscountPercentage() == null)
            user.setDiscountPercentage(oldUser.getDiscountPercentage());
    }

    private void setCardsAndAddress(Customer customer) {
        customer.getCreditCards().forEach(creditCard -> creditCard.setCustomer(customer));
        Address address = customer.getAddress();
        if (address.getId() == null) {
            address = addressRepo.save(address);
            customer.setAddress(address);
        }
    }
}
