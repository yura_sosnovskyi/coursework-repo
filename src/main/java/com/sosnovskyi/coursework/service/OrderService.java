package com.sosnovskyi.coursework.service;

import com.sosnovskyi.coursework.entity.Order;
import com.sosnovskyi.coursework.exception.EntityNotFoundException;
import com.sosnovskyi.coursework.repo.OrderRepo;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Data
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    private final OrderRepo orderRepo;

    @Transactional
    public Order save(Order order) {
        log.info("save Order with {} id...", order.getId());

        Order savedOrder = orderRepo.save(order);
        log.info("Order with {} id is successfully created", savedOrder.getId());
        return savedOrder;
    }

    @Transactional
    public Order findOrder(Integer id) {
        log.info("findOrder with {} id...", id);
        Order order = orderRepo.findById(id)
                .orElseThrow(EntityNotFoundException::new);
        log.info("Order with {} id is successfully found", order.getId());
        return order;
    }

    @Transactional
    public List<Order> findAll() {
        log.info("findAll Orders");
        List<Order> all = orderRepo.findAll();
        log.info("order list with {} elements found", all.size());
        return all;
    }

    @Transactional
    public void deleteOrder(Integer id) {
        log.info("deleteOrder with id {}", id);
        Order order = orderRepo.findById(id)
                .orElseThrow(EntityNotFoundException::new);
        orderRepo.delete(order);
        log.info("Order with {} id is successfully deleted", id);
    }
}
