package com.sosnovskyi.coursework.service;

import com.sosnovskyi.coursework.entity.Staff;
import com.sosnovskyi.coursework.exception.EntityNotFoundException;
import com.sosnovskyi.coursework.repo.StaffRepo;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Data
@RequiredArgsConstructor
@Slf4j
public class StaffService {

    private final StaffRepo staffRepo;


    @Transactional
    public Staff save(Staff staff) {
        log.info("save Staff with {} email...", staff.getEmail());

        Staff savedStaff = staffRepo.save(staff);
        log.info("Staff with {} email, {} id is successfully created",
                savedStaff.getEmail(), savedStaff.getId());
        return savedStaff;
    }

    @Transactional
    public Staff findStaff(String email) {
        log.info("findStaff with {} email...", email);
        Staff staff = staffRepo.findByEmail(email)
                .orElseThrow(EntityNotFoundException::new);
        log.info("Staff with {} email, {} id is successfully found",
                staff.getEmail(), staff.getId());
        return staff;
    }

    @Transactional
    public List<Staff> findAll() {
        log.info("findAll Staffs");
        List<Staff> all = staffRepo.findAll();
        log.info("staff list with {} elements found", all.size());
        return all;
    }

    @Transactional
    public Staff updateStaff(String email, Staff staff) {
        log.info("updateStaff with email {}", email);

        Staff resStaff = staffRepo.findByEmail(email)
                .map(staffer -> {
                    staffer.setSurname(staff.getSurname());
                    staffer.setStaffRole(staff.getStaffRole());
                    staffer.setDismissalDate(staff.getDismissalDate());
                    staffer.setGasStation(staff.getGasStation());
                    staffer.setPhone(staff.getPhone());
                    staffer.setIsActive(staff.getIsActive());
                    staffer.setRating(staff.getRating());
                    staffer.setSalaryPayments(staff.getSalaryPayments());
                    return staffRepo.save(staffer);
                })
                .orElseGet(() -> staffRepo.save(staff));
        log.info("staff with {} email is successfully updated", resStaff.getEmail());
        return resStaff;
    }

    @Transactional
    public void deleteStaff(String email) {
        log.info("deleteStaff with email {}", email);
        Staff staff = staffRepo.findByEmail(email)
                .orElseThrow(EntityNotFoundException::new);
        staffRepo.delete(staff);
        log.info("Staff with {} email is successfully deleted", email);
    }
}
