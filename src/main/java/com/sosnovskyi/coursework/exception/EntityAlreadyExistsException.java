package com.sosnovskyi.coursework.exception;

import com.sosnovskyi.coursework.exception.enums.ErrorType;

public class EntityAlreadyExistsException extends ServiceException {

    private static final String DEFAULT_MESSAGE = "Entity is already exist!";

    public EntityAlreadyExistsException() {
        super(DEFAULT_MESSAGE);
    }

    @Override
    public ErrorType getErrorType() {
        return ErrorType.VALIDATION_ERROR_TYPE;
    }
}
