package com.sosnovskyi.coursework.exception;

import com.sosnovskyi.coursework.exception.enums.ErrorType;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class ServiceError {

    private String message;
    private ErrorType errorType;
    private LocalDateTime timeStamp;

}
