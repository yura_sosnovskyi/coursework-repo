package com.sosnovskyi.coursework.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;

@Table(name = "opt_purchase")
@Entity
public class OptPurchase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "opt_purchase_id", nullable = false)
    private Integer id;

    @Column(name = "opt_price", nullable = false, precision = 10, scale = 2)
    private BigDecimal optPrice;

    @Column(name = "opt_quantity", nullable = false)
    private Integer optQuantity;

    @ManyToOne
    @JoinColumn(name = "gas_station_id")
    private GasStation gasStation;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @Column(name = "created_at", insertable = false, updatable = false)
    private Instant createdAt;

    @Column(name = "updated_at", insertable = false, updatable = false)
    private Instant updatedAt;

    @Column(name = "created_by", insertable = false, updatable = false, length = 30)
    private String createdBy;

    @Column(name = "updated_by", insertable = false, updatable = false, length = 30)
    private String updatedBy;

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public GasStation getGasStation() {
        return gasStation;
    }

    public void setGasStation(GasStation gasStation) {
        this.gasStation = gasStation;
    }

    public Integer getOptQuantity() {
        return optQuantity;
    }

    public void setOptQuantity(Integer optQuantity) {
        this.optQuantity = optQuantity;
    }

    public BigDecimal getOptPrice() {
        return optPrice;
    }

    public void setOptPrice(BigDecimal optPrice) {
        this.optPrice = optPrice;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}