package com.sosnovskyi.coursework.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class StockId implements Serializable {

    private static final long serialVersionUID = -5326082188357929506L;

    @Column(name = "gas_station_id", nullable = false)
    private Integer gasStationId;

    @Column(name = "product_id", nullable = false)
    private Integer productId;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getGasStationId() {
        return gasStationId;
    }

    public void setGasStationId(Integer gasStationId) {
        this.gasStationId = gasStationId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, gasStationId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || org.springframework.data.util.ProxyUtils.getUserClass(this) != org.springframework.data.util.ProxyUtils.getUserClass(o))
            return false;
        StockId entity = (StockId) o;
        return Objects.equals(this.productId, entity.productId) &&
                Objects.equals(this.gasStationId, entity.gasStationId);
    }
}