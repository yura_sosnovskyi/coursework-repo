package com.sosnovskyi.coursework.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.Instant;

@Table(name = "staff_role")
@Entity
@Data
public class StaffRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "staff_role_id", nullable = false)
    private Integer id;

    @Column(name = "staff_role_name", nullable = false, length = 50)
    private String staffRoleName;

    @Column(name = "created_at", insertable = false, updatable = false)
    private Instant createdAt;

    @Column(name = "updated_at", insertable = false, updatable = false)
    private Instant updatedAt;

    @Column(name = "created_by", insertable = false, updatable = false, length = 30)
    private String createdBy;

    @Column(name = "updated_by", insertable = false, updatable = false, length = 30)
    private String updatedBy;
}