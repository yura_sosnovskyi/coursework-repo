package com.sosnovskyi.coursework.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@Table(name = "staff")
@Entity
@Data
public class Staff {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "staff_id", nullable = false)
    private Integer id;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY, optional = false, cascade = {CascadeType.ALL})
    @JoinColumn(name = "gas_station_id", nullable = false)
    private GasStation gasStation;

    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @Column(name = "surname", nullable = false, length = 50)
    private String surname;

    @Column(name = "email", nullable = false, length = 50)
    private String email;

    @Column(name = "phone", length = 20)
    private String phone;

    @ManyToOne(optional = false)
    @JoinColumn(name = "staff_role_id", nullable = false)
    private StaffRole staffRole;

    @JsonManagedReference
    @OneToMany(mappedBy = "staff", cascade = {CascadeType.ALL})
    private List<SalaryPayment> salaryPayments;

    @Column(name = "is_active", nullable = false)
    private Integer isActive;

    @Column(name = "rating", precision = 2, scale = 1)
    private BigDecimal rating;

    @Column(name = "employment_date", nullable = false)
    private Instant employmentDate;

    @Column(name = "dismissal_date")
    private Instant dismissalDate;

    @Column(name = "created_at", insertable = false, updatable = false)
    private Instant createdAt;

    @Column(name = "updated_at", insertable = false, updatable = false)
    private Instant updatedAt;

    @Column(name = "created_by", insertable = false, updatable = false, length = 30)
    private String createdBy;

    @Column(name = "updated_by", insertable = false, updatable = false, length = 30)
    private String updatedBy;

}