package com.sosnovskyi.coursework.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Table(name = "gas_station", indexes = {
        @Index(name = "UQ__gas_stat__B43B145F2B347CF9", columnList = "phone", unique = true),
        @Index(name = "UQ__gas_stat__AB6E61640F0461D6", columnList = "email", unique = true),
        @Index(name = "UQ__gas_stat__CAA247C965E2B00F", columnList = "address_id", unique = true)
})
@Entity
@Data
public class GasStation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "gas_station_id", nullable = false)
    private Integer id;

    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @Column(name = "email", nullable = false, length = 50)
    private String email;

    @Column(name = "phone", length = 20)
    private String phone;

    @OneToMany(mappedBy = "gasStation")
    List<Stock> stocks;

    @JsonManagedReference
    @OneToMany(mappedBy="gasStation", cascade = CascadeType.MERGE)
    private List<Staff> staff;

    @OneToOne(cascade = CascadeType.MERGE, optional = false)
    @JoinColumn(name = "address_id", nullable = false)
    private Address address;

    @Column(name = "created_at", insertable = false, updatable = false)
    private Instant createdAt;

    @Column(name = "updated_at", insertable = false, updatable = false)
    private Instant updatedAt;

    @Column(name = "created_by", insertable = false, updatable = false, length = 30)
    private String createdBy;

    @Column(name = "updated_by", insertable = false, updatable = false, length = 30)
    private String updatedBy;

}